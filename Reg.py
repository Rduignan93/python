import re #regular expression library

#search and match - 2 ways to search for regular expressions
#search- gives boolean return value
#match-used when you want to use what youve found

peopleFH=open("Data.txt","r")
ptn=re.compile("^.*ati. ", re.IGNORECASE)

for person in peopleFH:
    #if re.search("22",person): #like grep boolean return value
    if ptn.search(person):
        print(person.rstrip("\r\n"))

peopleFH.seek(0,0)

for person in peopleFH:
    matched=re.search("^.*ati. ",person)
    if matched:
        print("Matched at",matched.span()) #shows location where match is found
        print("words found : ",matched.group()) # shows what was found
