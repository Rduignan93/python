
import re
import shutil

spaceptn=re.compile("\s+")
commentptn=re.compile("#.*")
MACptn=re.compile("([a-fA-F0-9]{2}[:|\-]?){6}")
ipaddressptn=re.compile("^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")


fh=open("hosts.real","r")
fh2=open("temp.txt","w")
fh3=open("ipaddress.txt","w")

fh.readline()

for line in fh:

    if re.search('#',line):
        line=commentptn.sub("",line)


    macadd=MACptn.search(line)
    if macadd:
        print(macadd.group().replace(",",""))

    ipadd=ipaddressptn.search(line)
    if ipadd:
        fh3.write(ipadd.group()+"\n")

    data=spaceptn.split(line)
    fh2.write(" ".join(data)+"\n")


fh.close()
fh2.close()
fh3.close()

shutil.move("temp.txt", "hosts.real")
