import re

opraFH=open("opra_example_regression.log","r")
rp=tt=tp=tv=""

rpre=re.compile("Record Publish")
ttre=re.compile("Type: Trade")
tpre=re.compile("wTradePrice")
tvre=re.compile("wTradeVolume")
reptn=re.compile("Regression:")

#open the file to search

for line in opraFH:

    if re.search('Regression:',line):
        line=reptn.sub("",line)
    if rpre.search(line):
        rp=line
    if ttre.search(line):
        tt=line
    if rp and tt:
        if tpre.search(line):
            tp=line
        if tvre.search(line):
            tv=line
    if rp and tt and tp and tv:
        print(rp.rstrip("\r\n")+tt.rstrip("\r\n"))
        print(tp+tv)
        rp=tt=tp=tv=""

        


opraFH.close()
